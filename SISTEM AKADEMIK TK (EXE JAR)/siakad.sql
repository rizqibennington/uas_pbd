-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2019 at 09:49 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siakad`
--

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `nip` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nomorHp` varchar(255) NOT NULL,
  `jenisKelamin` varchar(255) NOT NULL,
  `tanggalLahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `agama` varchar(255) NOT NULL,
  `mengajar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `nama`, `nomorHp`, `jenisKelamin`, `tanggalLahir`, `alamat`, `agama`, `mengajar`) VALUES
('170', 'sukmi', '081321233241', 'Wanita', '2019-12-12', 'bogor', 'Kristen', 'Menulis'),
('173', 'Dodi', '085723122232', 'Pria', '2019-12-31', 'Karawang', 'Islam', 'Berhitung'),
('1734', 'rizky rahmanasyah', '085678967', 'Pria', '2019-12-02', 'palembang', 'Islam', 'Menulis'),
('174', 'Santoso', '089822389094', 'Pria', '2019-12-04', 'Karawang', 'Islam', 'Menggambar');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `idSiswa` varchar(255) NOT NULL,
  `namaSiswa` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `nilaiMenggambar` int(11) NOT NULL,
  `nilaiBerhitung` int(11) NOT NULL,
  `nilaiMembaca` int(11) NOT NULL,
  `nilaiMenulis` int(11) NOT NULL,
  `nilaiMengaji` int(11) NOT NULL,
  `tahunAjaran` varchar(255) NOT NULL,
  `rataRata` float NOT NULL,
  `keterangan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`idSiswa`, `namaSiswa`, `kelas`, `semester`, `nilaiMenggambar`, `nilaiBerhitung`, `nilaiMembaca`, `nilaiMenulis`, `nilaiMengaji`, `tahunAjaran`, `rataRata`, `keterangan`) VALUES
('1131', 'Akbar', 'A1', '2', 100, 100, 100, 100, 100, '2018/2019', 100, 'A'),
('1132', 'Sholeh', 'A2', '3', 20, 20, 30, 40, 100, '2018/2019', 42, 'C'),
('1133', 'Wildan', 'B1', '1', 100, 10, 10, 90, 100, '2017/2018', 62, 'B'),
('1134', 'Dyas', 'B2', '4', 10, 10, 10, 10, 10, '10', 10, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `print`
--

CREATE TABLE `print` (
  `id` varchar(255) NOT NULL,
  `idSiswa` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `print`
--

INSERT INTO `print` (`id`, `idSiswa`) VALUES
('1', '1131');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `idSiswa` varchar(255) NOT NULL,
  `namaSiswa` varchar(255) NOT NULL,
  `jenisKelamin` enum('Pria','Wanita') NOT NULL,
  `tanggalLahir` date NOT NULL,
  `alamat` text NOT NULL,
  `agama` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `namaWali` varchar(255) NOT NULL,
  `noHpWali` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`idSiswa`, `namaSiswa`, `jenisKelamin`, `tanggalLahir`, `alamat`, `agama`, `kelas`, `semester`, `namaWali`, `noHpWali`) VALUES
('1131', 'Akbar', 'Pria', '2013-06-04', 'Jakarta', 'Islam', 'A1', '2', 'Hartono', '082267335231'),
('1132', 'Sholeh', 'Wanita', '2019-12-03', 'Bekasi', 'Hindu', 'A2', '3', 'Wanda', '12345'),
('1133', 'Wildan', 'Pria', '2011-12-04', 'Banten', 'Kristen', 'B1', '1', 'Widyan', '45786523'),
('1134', 'Dyas', 'Wanita', '2019-12-02', 'SOlo', 'Budha', 'B2', '4', 'Marlin', '0878983232');

-- --------------------------------------------------------

--
-- Table structure for table `tabungan`
--

CREATE TABLE `tabungan` (
  `idSiswa` varchar(11) NOT NULL,
  `namaSiswa` varchar(11) NOT NULL,
  `uangMasuk` int(11) NOT NULL,
  `uangKeluar` int(11) NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabungan`
--

INSERT INTO `tabungan` (`idSiswa`, `namaSiswa`, `uangMasuk`, `uangKeluar`, `saldo`) VALUES
('1134', 'Dyas', 13000, 10000, 3000);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `level` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `nama`, `level`) VALUES
('17190', '123', 'rizqi', 'Guru'),
('admin', 'admin', 'ADMINISTRATOR', 'Staff');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`idSiswa`);

--
-- Indexes for table `print`
--
ALTER TABLE `print`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`idSiswa`);

--
-- Indexes for table `tabungan`
--
ALTER TABLE `tabungan`
  ADD PRIMARY KEY (`idSiswa`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`idSiswa`) REFERENCES `siswa` (`idSiswa`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
