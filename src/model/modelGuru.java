/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.koneksiDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class modelGuru {
    private String nipModel;
    private String namaGuruModel;
    private String nomorHpModel;
    private String jenisKelaminModel;
    private String tanggalLahirModel;
    private String alamatModel;
    private String agamaModel;
    private String mengajarModel;
    
    koneksiDatabase koneksi = new koneksiDatabase();
    public modelGuru() {
    
    }

    public String getNipModel() {
        return nipModel;
    }

    public void setNipModel(String nipModel) {
        this.nipModel = nipModel;
    }

    public String getNamaGuruModel() {
        return namaGuruModel;
    }

    public void setNamaGuruModel(String namaGuruModel) {
        this.namaGuruModel = namaGuruModel;
    }

    public String getNomorHpModel() {
        return nomorHpModel;
    }

    public void setNomorHpModel(String nomorHpModel) {
        this.nomorHpModel = nomorHpModel;
    }

    public String getJenisKelaminModel() {
        return jenisKelaminModel;
    }

    public void setJenisKelaminModel(String jenisKelaminModel) {
        this.jenisKelaminModel = jenisKelaminModel;
    }

    public String getTanggalLahirModel() {
        return tanggalLahirModel;
    }

    public void setTanggalLahirModel(String tanggalLahirModel) {
        this.tanggalLahirModel = tanggalLahirModel;
    }

    public String getAlamatModel() {
        return alamatModel;
    }

    public void setAlamatModel(String alamatModel) {
        this.alamatModel = alamatModel;
    }

    public String getAgamaModel() {
        return agamaModel;
    }

    public void setAgamaModel(String agamaModel) {
        this.agamaModel = agamaModel;
    }

    public String getMengajarModel() {
        return mengajarModel;
    }

    public void setMengajarModel(String mengajarModel) {
        this.mengajarModel = mengajarModel;
    }

   

    public koneksiDatabase getKoneksi() {
        return koneksi;
    }

    public void setKoneksi(koneksiDatabase koneksi) {
        this.koneksi = koneksi;
    }
    
    
    public void simpanDataGuru(){
        //inisialisasi query
        String sql = ("INSERT INTO guru (nip, nama, nomorHp, jenisKelamin, tanggalLahir, alamat, agama, mengajar) "
                + "VALUES('"+getNipModel()+"', '"+getNamaGuruModel()+"', '"+getNomorHpModel()+"'"
                + ", '"+getJenisKelaminModel()+"', '"+getTanggalLahirModel()+"', '"+getAlamatModel()+"'"
                + ", '"+getAgamaModel()+"', '"+getMengajarModel()+"')");
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Disimpan \n "+ex);
        }
    }
    
    public void ubahDataGuru(){
        //inisialisasi query
        String sql = " UPDATE guru SET nama = '"+getNamaGuruModel()+"'"
                +",nomorHp = '"+getNomorHpModel()+"'"
                +",jenisKelamin = '"+getJenisKelaminModel()+"'"
                +",tanggalLahir = '"+getTanggalLahirModel()+"'"
                +",alamat = '"+getAlamatModel()+"'" 
                +",agama = '"+getAgamaModel()+"'"
                +",mengajar = '"+getMengajarModel()+"' WHERE nip = '"+getNipModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Diubah \n "+ex);
        }
    }
    
    public void hapusDataGuru(){
        //inisialisasi query
        String sql = " DELETE FROM guru WHERE nip = "
                + " '"+getNipModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Dihapus \n "+ex);
        }
    }
    public void hapusAkunGuru(){
        //inisialisasi query
        String sql = " DELETE FROM user WHERE username = "
                + " '"+getNipModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
//            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, "Data Gagal Dihapus \n "+ex);
        }
    }
    
}
