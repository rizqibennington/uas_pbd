/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.koneksiDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class modelInputNilai {
    private String idSiswaModel;
    private String namaSiswaModel;
    private String kelasModel;
    private String semesterModel;
    private int nilaiMenggambarModel;
    private int nilaiBerhitungModel;
    private int nilaiMembacaModel;
    private int nilaiMenulisModel;
    private int nilaiMengajiModel;
    private String tahunAjaranModel;
    private float rataRataModel;
    private String keteranganModel;
    
    koneksiDatabase koneksi = new koneksiDatabase();

    public String getIdSiswaModel() {
        return idSiswaModel;
    }

    public void setIdSiswaModel(String idSiswaModel) {
        this.idSiswaModel = idSiswaModel;
    }

    public String getNamaSiswaModel() {
        return namaSiswaModel;
    }

    public void setNamaSiswaModel(String namaSiswaModel) {
        this.namaSiswaModel = namaSiswaModel;
    }

    public String getKelasModel() {
        return kelasModel;
    }

    public void setKelasModel(String kelasModel) {
        this.kelasModel = kelasModel;
    }

    public String getSemesterModel() {
        return semesterModel;
    }

    public void setSemesterModel(String semesterModel) {
        this.semesterModel = semesterModel;
    }

    public int getNilaiMenggambarModel() {
        return nilaiMenggambarModel;
    }

    public void setNilaiMenggambarModel(int nilaiMenggambarModel) {
        this.nilaiMenggambarModel = nilaiMenggambarModel;
    }

    public int getNilaiBerhitungModel() {
        return nilaiBerhitungModel;
    }

    public void setNilaiBerhitungModel(int nilaiBerhitungModel) {
        this.nilaiBerhitungModel = nilaiBerhitungModel;
    }

    public int getNilaiMembacaModel() {
        return nilaiMembacaModel;
    }

    public void setNilaiMembacaModel(int nilaiMembacaModel) {
        this.nilaiMembacaModel = nilaiMembacaModel;
    }

    public int getNilaiMenulisModel() {
        return nilaiMenulisModel;
    }

    public void setNilaiMenulisModel(int nilaiMenulisModel) {
        this.nilaiMenulisModel = nilaiMenulisModel;
    }

    public int getNilaiMengajiModel() {
        return nilaiMengajiModel;
    }

    public void setNilaiMengajiModel(int nilaiMengajiModel) {
        this.nilaiMengajiModel = nilaiMengajiModel;
    }

    public String getTahunAjaranModel() {
        return tahunAjaranModel;
    }

    public void setTahunAjaranModel(String tahunAjaranModel) {
        this.tahunAjaranModel = tahunAjaranModel;
    }

    public float getRataRataModel() {
        return rataRataModel;
    }

    public void setRataRataModel(float rataRataModel) {
        this.rataRataModel = rataRataModel;
    }

    public String getKeteranganModel() {
        return keteranganModel;
    }

    public void setKeteranganModel(String keteranganModel) {
        this.keteranganModel = keteranganModel;
    }

   public void inputDataNilai(){
        //inisialisasi query
        String sql = ("INSERT INTO nilai (idSiswa, namaSiswa, kelas, semester, nilaiMenggambar, nilaiBerhitung, nilaiMembaca, nilaiMenulis, nilaiMengaji, "
                + "tahunAjaran, rataRata, keterangan) "
                + "VALUES('"+getIdSiswaModel()+"', '"+getNamaSiswaModel()+"', '"+getKelasModel()+"', '"+getSemesterModel()+"', '"+getNilaiMenggambarModel()+"'"
                + ", '"+getNilaiBerhitungModel()+"', '"+getNilaiMembacaModel()+"', '"+getNilaiMenulisModel()+"', '"+getNilaiMengajiModel()+"', "
                + "'"+getTahunAjaranModel()+"', '"+getRataRataModel()+"', '"+getKeteranganModel()+"')");
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Nilai Berhasil Diinput");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Nilai Gagal Diinput \n "+ex);
        }
    }
    
    public void updateDataNilai(){
        //inisialisasi query
        String sql = " UPDATE nilai SET namaSiswa = '"+getNamaSiswaModel()+"'"
                +",kelas = '"+getKelasModel()+"'"
                +",semester = '"+getSemesterModel()+"'"
                +",nilaiMenggambar = '"+getNilaiMenggambarModel()+"'"
                +",nilaiBerhitung = '"+getNilaiBerhitungModel()+"'"
                +",nilaiMembaca = '"+getNilaiMembacaModel()+"'"
                +",nilaiMenulis = '"+getNilaiMenulisModel()+"'"
                +",nilaiMengaji = '"+getNilaiMengajiModel()+"'"
                +",tahunAjaran = '"+getTahunAjaranModel()+"'"
                +",rataRata = '"+getRataRataModel()+"'"
                +",keterangan = '"+getKeteranganModel()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Nilai Berhasil Diupdate");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Nilai Gagal Diupdate \n "+ex);
        }
    }
    
     public void deleteDataNilai(){
        //inisialisasi query
        String sql = " DELETE FROM nilai WHERE idSiswa = "
                + " '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Nilai Berhasil Didelete");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Nilai Gagal Didelete \n "+ex);
        }
    }
    
    
  
    
}
