/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.koneksiDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class modelSiswa {
    private String idSiswaModel;
    private String namaSiswaModel;
    private String jenisKelaminModel;
    private String tanggalLahirModel;
    private String alamatModel;
    private String agamaModel;
    private String kelasModel;
    private String semesterModel;
    private String namaWaliModel;
    private String noHpWaliModel;
    
    koneksiDatabase koneksi = new koneksiDatabase();
    
    public modelSiswa(){
        
    }

    public String getIdSiswaModel() {
        return idSiswaModel;
    }

    public void setIdSiswaModel(String idSiswaModel) {
        this.idSiswaModel = idSiswaModel;
    }

    public String getNamaSiswaModel() {
        return namaSiswaModel;
    }

    public void setNamaSiswaModel(String namaSiswaModel) {
        this.namaSiswaModel = namaSiswaModel;
    }

    public String getJenisKelaminModel() {
        return jenisKelaminModel;
    }

    public void setJenisKelaminModel(String jenisKelaminModel) {
        this.jenisKelaminModel = jenisKelaminModel;
    }

    public String getTanggalLahirModel() {
        return tanggalLahirModel;
    }

    public void setTanggalLahirModel(String tanggalLahirModel) {
        this.tanggalLahirModel = tanggalLahirModel;
    }

    public String getAlamatModel() {
        return alamatModel;
    }

    public void setAlamatModel(String alamatModel) {
        this.alamatModel = alamatModel;
    }

    public String getAgamaModel() {
        return agamaModel;
    }

    public void setAgamaModel(String agamaModel) {
        this.agamaModel = agamaModel;
    }

    public String getKelasModel() {
        return kelasModel;
    }

    public void setKelasModel(String kelasModel) {
        this.kelasModel = kelasModel;
    }

    public String getNamaWaliModel() {
        return namaWaliModel;
    }

    public void setNamaWaliModel(String namaWaliModel) {
        this.namaWaliModel = namaWaliModel;
    }

    public String getNoHpWaliModel() {
        return noHpWaliModel;
    }

    public void setNoHpWaliModel(String noHpWaliModel) {
        this.noHpWaliModel = noHpWaliModel;
    }

    public String getSemesterModel() {
        return semesterModel;
    }

    public void setSemesterModel(String semesterModel) {
        this.semesterModel = semesterModel;
    }
    
    
    
    public void simpanDataSiswa(){
        //inisialisasi query
        String sql = ("INSERT INTO siswa (idSiswa, namaSiswa, jenisKelamin, tanggalLahir, alamat, agama, kelas, semester, namaWali, noHpWali) "
                + "VALUES('"+getIdSiswaModel()+"', '"+getNamaSiswaModel()+"', '"+getJenisKelaminModel()+"'"
                + ", '"+getTanggalLahirModel()+"', '"+getAlamatModel()+"', '"+getAgamaModel()+"'"
                + ", '"+getKelasModel()+"', '"+getSemesterModel()+"', '"+getNamaWaliModel()+"', '"+getNoHpWaliModel()+"')");
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Disimpan \n "+ex);
        }
    }
    
    public void ubahDataSiswa(){
        //inisialisasi query
        String sql = " UPDATE siswa SET namaSiswa = '"+getNamaSiswaModel()+"'"
                +",jenisKelamin = '"+getJenisKelaminModel()+"'"
                +",tanggalLahir = '"+getTanggalLahirModel()+"'"
                +",alamat = '"+getAlamatModel()+"'" 
                +",agama = '"+getAgamaModel()+"'"
                +",kelas = '"+getKelasModel()+"'"
                +",semester = '"+getSemesterModel()+"'"
                +",namaWali = '"+getNamaWaliModel()+"'"
                +",noHpWali = '"+getNoHpWaliModel()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Diubah \n "+ex);
        }
    }
    
    public void ubahDataNilai(){
        //inisialisasi query
        String sql = " UPDATE nilai SET namaSiswa = '"+getNamaSiswaModel()+"'"
                +",kelas = '"+getKelasModel()+"'"
                +",semester = '"+getSemesterModel()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
//            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, "Data Gagal Diubah \n "+ex);
        }
    }
    
    public void ubahDataTabungan(){
        //inisialisasi query
        String sql = " UPDATE tabungan SET namaSiswa = '"+getNamaSiswaModel()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
//            JOptionPane.showMessageDialog(null, "Data Berhasil Diubah");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, "Data Gagal Diubah \n "+ex);
        }
    }
    
    
    
    public void hapusDataSiswa(){
        //inisialisasi query
        String sql = " DELETE FROM siswa WHERE idSiswa = "
                + " '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Data Gagal Dihapus \n "+ex);
        }
    }
    
     public void hapusDatanilai(){
        //inisialisasi query
        String sql = " DELETE FROM nilai WHERE idSiswa = "
                + " '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
//            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, "Data Gagal Dihapus \n "+ex);
        }
    }
      public void hapusDataTabungan(){
        //inisialisasi query
        String sql = " DELETE FROM tabungan WHERE idSiswa = "
                + " '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
//            JOptionPane.showMessageDialog(null, "Data Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
//            JOptionPane.showMessageDialog(null, "Data Gagal Dihapus \n "+ex);
        }
    }
     
}
