/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.koneksiDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class modelTabungan {
    private String idSiswaModel, namaSiswaModel;
    private int jumlahSetorModel, jumlahTarikModel, saldo;
    
    koneksiDatabase koneksi = new koneksiDatabase();

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    
    
    public String getIdSiswaModel() {
        return idSiswaModel;
    }

    public void setIdSiswaModel(String idSiswaModel) {
        this.idSiswaModel = idSiswaModel;
    }

    public String getNamaSiswaModel() {
        return namaSiswaModel;
    }

    public void setNamaSiswaModel(String namaSiswaModel) {
        this.namaSiswaModel = namaSiswaModel;
    }

    public int getJumlahSetorModel() {
        return jumlahSetorModel;
    }

    public void setJumlahSetorModel(int jumlahSetorModel) {
        this.jumlahSetorModel = jumlahSetorModel;
    }

    public int getJumlahTarikModel() {
        return jumlahTarikModel;
    }

    public void setJumlahTarikModel(int jumlahTarikModel) {
        this.jumlahTarikModel = jumlahTarikModel;
    }

    public koneksiDatabase getKoneksi() {
        return koneksi;
    }

    public void setKoneksi(koneksiDatabase koneksi) {
        this.koneksi = koneksi;
    }
    
    public void setorBaru(){
        String sql = ("INSERT INTO tabungan (idSiswa, namaSiswa, uangMasuk, uangKeluar, saldo) "
                + "VALUES('"+getIdSiswaModel()+"', '"+getNamaSiswaModel()+"', '"+getJumlahSetorModel()+"', 0, '"+getSaldo()+"')");
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Tabungan berhasil disetorkan");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Tabungan gagal disetorkan \n "+ex);
        }
    }
    
    public void tarik(){
        String sql = " UPDATE tabungan SET uangKeluar = '"+getJumlahTarikModel()+"', saldo = '"+getSaldo()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Tabungan berhasil ditarik");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Tabungan gagal ditarik \n "+ex);
        }
    }
    
    public void setorTambah(){
        String sql = " UPDATE tabungan SET uangMasuk = '"+getJumlahSetorModel()+"', saldo = '"+getSaldo()+"' WHERE idSiswa = '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Tabungan berhasil disetorkan");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Tabungan gagal disetorkan \n "+ex);
        }
    }
    
    public void hapusTabungan(){
        //inisialisasi query
        String sql = " DELETE FROM tabungan WHERE idSiswa = "
                + " '"+getIdSiswaModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Tabungan Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Tabungan Gagal Dihapus \n "+ex);
        }
    }
    
}
