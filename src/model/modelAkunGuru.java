/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import db.koneksiDatabase;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author ASUS
 */
public class modelAkunGuru {
    private String usernameModel;
    private String passwordModel, namaModel;
    
    koneksiDatabase koneksi = new koneksiDatabase();
    public modelAkunGuru(){
        
    }

    public String getUsernameModel() {
        return usernameModel;
    }

    public void setUsernameModel(String usernameModel) {
        this.usernameModel = usernameModel;
    }

    public String getPasswordModel() {
        return passwordModel;
    }

    public void setPasswordModel(String passwordModel) {
        this.passwordModel = passwordModel;
    }

    public String getNamaModel() {
        return namaModel;
    }

    public void setNamaModel(String namaModel) {
        this.namaModel = namaModel;
    }
    
    
    
    public void buatDataAkunGuru(){
        String sql = ("INSERT INTO user (username, password, nama, level) "
                + "VALUES('"+getUsernameModel()+"', '"+getPasswordModel()+"', '"+getNamaModel()+"', 'Guru')");
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Akun Berhasil Disimpan");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Akun Gagal Disimpan \n "+ex);
        }
    }
     public void ubahDataAkunGuru(){
        //inisialisasi query
        String sql = " UPDATE user SET password = '"+getPasswordModel()+"' WHERE username = '"+getUsernameModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Akun Berhasil Diubah");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Akun Gagal Diubah \n "+ex);
        }
    }
     
     public void hapusDataGuru(){
        //inisialisasi query
        String sql = " DELETE FROM user WHERE username = "
                + " '"+getUsernameModel()+"'";
        try {
            //inisialisasi statement
            PreparedStatement eksekusi = koneksi.getKoneksi().prepareStatement(sql);
            eksekusi.execute();
            
            //pemberitahuan jika data berhasil disimpan
            JOptionPane.showMessageDialog(null, "Akun Berhasil Dihapus");
        } catch (SQLException ex) {
            //Logger.getLogger(modelPelanggan.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Akun Gagal Dihapus \n "+ex);
        }
    }
}
