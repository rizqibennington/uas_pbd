/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.modelAkunGuru;
import view.viewAkunGuru;

/**
 *
 * @author ASUS
 */
public class controllerAkunGuru {
    

    private viewAkunGuru vA;
    private modelAkunGuru mA;
    
    public controllerAkunGuru(viewAkunGuru vA){
        this.vA = vA;
    }
    
public void kontrolbutton(){
    vA.getTombolUbah().setEnabled(false);
    vA.getTombolHapus().setEnabled(false);
}

public void kontrolbutton2(){
    vA.getTombolUbah().setEnabled(true);
    vA.getTombolHapus().setEnabled(true);
    vA.getTombolBuat().setEnabled(false);
}

public void bersihkan(){
    vA.getUsername().setText("");
    vA.getPassword().setText("");
    vA.getNamaGuru().setText("");
    vA.getTombolBuat().setEnabled(false);
    kontrolbutton();
}
    
public void buatAkunDataGuru(){
    mA = new modelAkunGuru();
    mA.setUsernameModel(vA.getUsername().getText());
    mA.setPasswordModel(vA.getPassword().getText());
    mA.setNamaModel(vA.getNamaGuru().getText());
    mA.buatDataAkunGuru();
    bersihkan();
}

public void ubahAkunDataGuru(){
    mA = new modelAkunGuru();
    mA.setUsernameModel(vA.getUsername().getText());
    mA.setPasswordModel(vA.getPassword().getText());
    mA.ubahDataAkunGuru();
    bersihkan();
}

public void hapusDataAkun(){
        mA = new modelAkunGuru();
        mA.setUsernameModel(vA.getUsername().getText());
        mA.hapusDataGuru();
        bersihkan();
    }


    
}
