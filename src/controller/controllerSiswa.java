/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.SimpleDateFormat;
import model.modelSiswa;
import view.viewSiswa;

/**
 *
 * @author ASUS
 */
public class controllerSiswa {
    private modelSiswa mS;
    private final viewSiswa vS;
    
    public controllerSiswa(viewSiswa vS){
        this.vS = vS;
    }
    
    public void kontrolbutton2(){
        vS.getTombolHapus().setEnabled(false);
        vS.getTombolUbah().setEnabled(false);
        vS.getTombolSimpan().setEnabled(true);
        vS.getIdSiswaView().setEnabled(true);
    }
    
    public void kontrolbutton(){
        vS.getIdSiswaView().setEnabled(false);
        vS.getTombolUbah().setEnabled(true);
        vS.getTombolHapus().setEnabled(true);
        vS.getTombolSimpan().setEnabled(false);
    }
    
    public void bersihkan(){
    vS.getIdSiswaView().setText("");
    vS.getNamaSiswaView().setText("");
    vS.getTanggalLahir().setDate(null);
    vS.getAlamatView().setText("");
    vS.getNamaWaliView().setText("");
    vS.getNoHpWaliView().setText("");
    vS.getSemester().setText("");
    vS.getPria().setSelected(false);
    vS.getWanita().setSelected(false);
    kontrolbutton2();
    }
    
    public void simpanDataSiswa(){
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mS = new modelSiswa();
        mS.setIdSiswaModel(vS.getIdSiswaView().getText());
        mS.setNamaSiswaModel(vS.getNamaSiswaView().getText());
        if(vS.getPria().isSelected()){
            mS.setJenisKelaminModel("Pria");
        }else if(vS.getWanita().isSelected()){
            mS.setJenisKelaminModel("Wanita");
        }
        mS.setTanggalLahirModel(sdf.format(vS.getTanggalLahir().getDate()));
        mS.setAlamatModel(vS.getAlamatView().getText());
        mS.setAgamaModel((String)vS.getAgamaView().getSelectedItem());
        mS.setKelasModel((String)vS.getKelasView().getSelectedItem());
        mS.setSemesterModel(vS.getSemester().getText());
        mS.setNamaWaliModel(vS.getNamaWaliView().getText());
        mS.setNoHpWaliModel(vS.getNoHpWaliView().getText());
        mS.simpanDataSiswa();
    }
    
    public void ubahDataSiswa(){
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mS = new modelSiswa();
        mS.setIdSiswaModel(vS.getIdSiswaView().getText());
        mS.setNamaSiswaModel(vS.getNamaSiswaView().getText());
        if(vS.getPria().isSelected()){
            mS.setJenisKelaminModel("Pria");
        }else if(vS.getWanita().isSelected()){
            mS.setJenisKelaminModel("Wanita");
        }
        mS.setTanggalLahirModel(sdf.format(vS.getTanggalLahir().getDate()));
        mS.setAlamatModel(vS.getAlamatView().getText());
        mS.setAgamaModel((String)vS.getAgamaView().getSelectedItem());
        mS.setKelasModel((String)vS.getKelasView().getSelectedItem());
        mS.setSemesterModel(vS.getSemester().getText());
        mS.setNamaWaliModel(vS.getNamaWaliView().getText());
        mS.setNoHpWaliModel(vS.getNoHpWaliView().getText());
        mS.ubahDataSiswa();
        mS.ubahDataNilai();
        mS.ubahDataTabungan();
    }
    
    public void hapusDataSiswa(){
        mS = new modelSiswa();
        mS.setIdSiswaModel(vS.getIdSiswaView().getText());
        mS.hapusDataSiswa();
        mS.hapusDatanilai();
        mS.hapusDataTabungan();
//        bersihkan();
    }
    
}
