/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import java.text.SimpleDateFormat;
import model.modelGuru;
import view.viewGuru;
/**
 *
 * @author ASUS
 */
public class controllerGuru {
    private modelGuru mG;
    private final viewGuru vG;
    
    public controllerGuru(viewGuru vG){
        this.vG = vG;
    }
    public void kontrolbutton2(){
        vG.getTombolHapus().setEnabled(false);
        vG.getTombolUbah().setEnabled(false);
        vG.getTombolSimpan().setEnabled(true);
        vG.getNip().setEnabled(true);
    }
    
    public void kontrolbutton(){
        vG.getNip().setEnabled(false);
        vG.getTombolUbah().setEnabled(true);
        vG.getTombolHapus().setEnabled(true);
        vG.getTombolSimpan().setEnabled(false);
    }
    public void bersihkan(){
    vG.getNip().setText("");
    vG.getNamaGuru().setText("");
    vG.getNomorHp().setText("");
    vG.getAlamat().setText("");
    vG.getTanggalLahir().setDate(null);
    kontrolbutton2();
    }
    
     public void simpanDataGuru(){
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mG = new modelGuru();
        mG.setNipModel(vG.getNip().getText());
        mG.setNamaGuruModel(vG.getNamaGuru().getText());
        mG.setNomorHpModel(vG.getNomorHp().getText());
      
        if(vG.getPria().isSelected()){
            mG.setJenisKelaminModel("Pria");
        }else if(vG.getWanita().isSelected()){
            mG.setJenisKelaminModel("Wanita");
        }
        mG.setTanggalLahirModel(sdf.format(vG.getTanggalLahir().getDate()));
        mG.setAlamatModel(vG.getAlamat().getText());
        mG.setAgamaModel((String)vG.getAgama().getSelectedItem());
        mG.setMengajarModel((String)vG.getMengajar().getSelectedItem());
        mG.simpanDataGuru();
     }
     
     public void ubahDataGuru(){
        SimpleDateFormat sdf;
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mG = new modelGuru();
        mG.setNipModel(vG.getNip().getText());
        mG.setNamaGuruModel(vG.getNamaGuru().getText());
        mG.setNomorHpModel(vG.getNomorHp().getText());
      
        if(vG.getPria().isSelected()){
            mG.setJenisKelaminModel("Pria");
        }else if(vG.getWanita().isSelected()){
            mG.setJenisKelaminModel("Wanita");
        }
        mG.setTanggalLahirModel(sdf.format(vG.getTanggalLahir().getDate()));
        mG.setAlamatModel(vG.getAlamat().getText());
        mG.setAgamaModel((String)vG.getAgama().getSelectedItem());
        mG.setMengajarModel((String)vG.getMengajar().getSelectedItem());
        mG.ubahDataGuru();
     }
     public void hapusData(){
        mG = new modelGuru();
        mG.setNipModel(vG.getNip().getText());
        mG.hapusDataGuru();
        mG.hapusAkunGuru();
        bersihkan();
    }
     
}
