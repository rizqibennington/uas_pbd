/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.modelInputNilai;
import view.viewInputNilai;

/**
 *
 * @author ASUS
 */
public class controllerInputNilai {
    private modelInputNilai mI;
    private viewInputNilai vI;
    
    public controllerInputNilai(viewInputNilai vI){
        this.vI = vI;
    }
    public void kontrolbutton(){
        vI.getTombolDelete().setEnabled(false);
        vI.getTombolUpdate().setEnabled(false);
        vI.getTombolInput().setEnabled(true);
    }
    
    public void kontrolbutton2(){
        vI.getTombolDelete().setEnabled(true);
        vI.getTombolUpdate().setEnabled(true);
        vI.getTombolInput().setEnabled(false);
    }
    
    public void bersihkan(){
        vI.getIdSiswaView().setText("");
        vI.getNamaSiswaView().setText("");
        vI.getKelasView().setText("");
        vI.getSemesterView().setText("");
        vI.getNilaiMenggambarView().setText("");
        vI.getNilaiBerhitungView().setText("");
        vI.getNilaiMengajiView().setText("");
        vI.getNilaiMembacaView().setText("");
        vI.getNilaiMenulisView().setText("");
        vI.getRataRataView().setText("");
        vI.getKeteranganView().setText("");
        vI.getTahunAjaranView().setText("");
        kontrolbutton();
        
    }
    public void input(){
        mI = new modelInputNilai();
        mI.setIdSiswaModel(vI.getIdSiswaView().getText());
        mI.setNamaSiswaModel(vI.getNamaSiswaView().getText());
        mI.setKelasModel(vI.getKelasView().getText());
        mI.setSemesterModel(vI.getSemesterView().getText());
        mI.setNilaiMenggambarModel(Integer.parseInt(vI.getNilaiMenggambarView().getText()));
        mI.setNilaiMenulisModel(Integer.parseInt(vI.getNilaiMenulisView().getText()));
        mI.setNilaiMembacaModel(Integer.parseInt(vI.getNilaiMembacaView().getText()));
        mI.setNilaiBerhitungModel(Integer.parseInt(vI.getNilaiBerhitungView().getText()));
        mI.setNilaiMengajiModel(Integer.parseInt(vI.getNilaiMengajiView().getText()));
        mI.setTahunAjaranModel(vI.getTahunAjaranView().getText());
        mI.setRataRataModel(Float.parseFloat(vI.getRataRataView().getText()));
        mI.setKeteranganModel(vI.getKeteranganView().getText());
        mI.inputDataNilai();
        bersihkan();
    }
    
    public void update(){
        mI = new modelInputNilai();
        mI.setIdSiswaModel(vI.getIdSiswaView().getText());
        mI.setNamaSiswaModel(vI.getNamaSiswaView().getText());
        mI.setKelasModel(vI.getKelasView().getText());
        mI.setSemesterModel(vI.getSemesterView().getText());
        mI.setNilaiMenggambarModel(Integer.parseInt(vI.getNilaiMenggambarView().getText()));
        mI.setNilaiMenulisModel(Integer.parseInt(vI.getNilaiMenulisView().getText()));
        mI.setNilaiMembacaModel(Integer.parseInt(vI.getNilaiMembacaView().getText()));
        mI.setNilaiBerhitungModel(Integer.parseInt(vI.getNilaiBerhitungView().getText()));
        mI.setNilaiMengajiModel(Integer.parseInt(vI.getNilaiMengajiView().getText()));
        mI.setTahunAjaranModel(vI.getTahunAjaranView().getText());
        mI.setRataRataModel(Float.parseFloat(vI.getRataRataView().getText()));
        mI.setKeteranganModel(vI.getKeteranganView().getText());
        mI.updateDataNilai();
        bersihkan();
    }
    
    public void delete(){
        mI = new modelInputNilai();
        mI.setIdSiswaModel(vI.getIdSiswaView().getText());
        mI.deleteDataNilai();
        bersihkan();
    }
}
