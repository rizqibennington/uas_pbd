/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.modelTabungan;
import view.viewTabungan;

/**
 *
 * @author ASUS
 */
public class controllerTabungan {
    private viewTabungan vT;
    private modelTabungan mT;
    private int hitungsaldo;
    private int hitungsetor;
    private int hitungtarik;
    public controllerTabungan(viewTabungan vT){
        this.vT = vT;
    }
    public void bersihkan(){
        vT.getJumlahSetor().setEnabled(false);
        vT.getJumlahTarik().setEnabled(false);
        vT.getSetorBaru().setEnabled(false);
        vT.getSetorTambah().setEnabled(false);
        vT.getProsesTarik().setEnabled(false);
        vT.getIdSiswaSetor().setText("");
        vT.getNamaSiswaSetor().setText("");
        vT.getJumlahSetor().setText("");
        vT.getIdSiswaTarik().setText("");
        vT.getNamaSiswaTarik().setText("");
        vT.getJumlahTarik().setText("");
    }
    public void setorBaru(){
         mT = new modelTabungan();
    mT.setIdSiswaModel(vT.getIdSiswaSetor().getText());
    mT.setNamaSiswaModel(vT.getNamaSiswaSetor().getText());
    mT.setJumlahSetorModel(Integer.parseInt(vT.getJumlahSetor().getText()));
    mT.setSaldo(Integer.parseInt(vT.getJumlahSetor().getText()));
    mT.setorBaru();
    bersihkan();
    }
    
    public void setorTambah(){
    mT = new modelTabungan();
    int index = vT.getTabelTabungan().getSelectedRow();
    String sld = String.valueOf(vT.getTabelTabungan().getValueAt(index, 2));
    hitungsaldo = Integer.parseInt(vT.getSaldo().getText()) + Integer.parseInt(vT.getJumlahSetor().getText());
    hitungsetor = Integer.parseInt(vT.getJumlahSetor().getText()) + Integer.parseInt(sld);
    mT.setIdSiswaModel(vT.getIdSiswaSetor().getText());
    mT.setJumlahSetorModel(hitungsetor);
    mT.setSaldo(hitungsaldo);
    mT.setorTambah();
    bersihkan();
    }
    
    public void tarik(){
    mT = new modelTabungan();
    int index = vT.getTabelTabungan().getSelectedRow();
    String keluar = String.valueOf(vT.getTabelTabungan().getValueAt(index, 3));
    hitungsaldo = Integer.parseInt(vT.getSaldo().getText()) - Integer.parseInt(vT.getJumlahTarik().getText());
    hitungtarik = Integer.parseInt(vT.getJumlahTarik().getText()) + Integer.parseInt(keluar);
    mT.setIdSiswaModel(vT.getIdSiswaTarik().getText());
    mT.setJumlahTarikModel(Integer.parseInt(vT.getJumlahTarik().getText()));
    mT.setSaldo(hitungsaldo);
    mT.tarik();
    bersihkan();
    }
    
    public void hapusTabungan(){
        mT = new modelTabungan();
        mT.setIdSiswaModel(vT.getIdSiswaSetor().getText());
        mT.hapusTabungan();
        bersihkan();
    }
    
}
