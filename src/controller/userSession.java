/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author ASUS
 */
public class userSession {
    private static String username, password, nama, level="kosong";

    public static String getLevel() {
        return level;
    }

    public static void setLevel(String level) {
        userSession.level = level;
    }

    public static String getNama() {
        return nama;
    }

    public static void setNama(String nama) {
        userSession.nama = nama;
    }
    
    
    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        userSession.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        userSession.password = password;
    }
    
}
