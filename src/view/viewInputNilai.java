/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.mysql.jdbc.Statement;
import controller.controllerInputNilai;
import db.koneksiDatabase;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ASUS
 */

public class viewInputNilai extends javax.swing.JInternalFrame {
    private int total;
    private float average;
    private controllerInputNilai cI;
    private DefaultTableModel model;
    /**
     * Creates new form viewInputNilai
     */
    public viewInputNilai() {
        
        initComponents();
        cI = new controllerInputNilai(this);
        model = new DefaultTableModel();
        tabelDataNilai.setModel(model);
        model.addColumn("ID");
        model.addColumn("Nama");
        model.addColumn("Kelas");
        model.addColumn("Semester");
        model.addColumn("Menggambar");
        model.addColumn("Berhitung");
        model.addColumn("Membaca");
        model.addColumn("Menulis");
        model.addColumn("Mengaji");
        model.addColumn("Rata-Rata");
        model.addColumn("Keterangan");
        model.addColumn("Tahun Ajaran");
        
        tampilDataNilai();
        cI.kontrolbutton();
    }

    public JTextField getIdSiswaView() {
        return idSiswaView;
    }

    public JTextField getKelasView() {
        return kelasView;
    }

    public JTextField getKeteranganView() {
        return keteranganView;
    }

    public JTextField getNamaSiswaView() {
        return namaSiswaView;
    }

    public JTextField getNilaiBerhitungView() {
        return nilaiBerhitungView;
    }

    public JTextField getNilaiMembacaView() {
        return nilaiMembacaView;
    }

    public JTextField getNilaiMengajiView() {
        return nilaiMengajiView;
    }

    public JTextField getNilaiMenggambarView() {
        return nilaiMenggambarView;
    }

    public JTextField getNilaiMenulisView() {
        return nilaiMenulisView;
    }

    public JTextField getRataRataView() {
        return rataRataView;
    }

    public JTextField getSemesterView() {
        return semesterView;
    }

    public JTextField getTahunAjaranView() {
        return tahunAjaranView;
    }

    public JButton getTombolCancel() {
        return tombolCancel;
    }

    public JButton getTombolDelete() {
        return tombolDelete;
    }

    public JButton getTombolUpdate() {
        return tombolUpdate;
    }

    public JButton getTombolInput() {
        return tombolInput;
    }
    
     private void tampilDataNilai(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
//        
        String sql = "SELECT * FROM nilai";
//        
        try {
            Statement stat = (Statement) koneksiDatabase.getKoneksi().createStatement();
            ResultSet res = stat.executeQuery(sql);
            
            
            while (res.next()){
                //mengambil hasil query variabel sql
                Object[] hasil;
                hasil = new Object[12];//karena ada 8 field
                hasil[0] = res.getString("idSiswa");
                hasil[1] = res.getString("namaSiswa");
                hasil[2] = res.getString("kelas");
                hasil[3] = res.getString("semester");
                hasil[4] = res.getString("nilaiMenggambar");
                hasil[5] = res.getString("nilaiBerhitung");
                hasil[6] = res.getString("nilaiMembaca");
                hasil[7] = res.getString("nilaiMenulis");
                hasil[8] = res.getString("nilaiMengaji");
                hasil[9] = res.getString("rataRata");
                hasil[10] = res.getString("keterangan");
                hasil[11] = res.getString("tahunAjaran");
                model.addRow(hasil);
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(viewInputNilai.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
     
     private void ambilDataTabel(){
        
        int index = tabelDataNilai.getSelectedRow();
        //mengambul nilai dari tabel
        String id = String.valueOf(tabelDataNilai.getValueAt(index, 0));
        String nama = String.valueOf(tabelDataNilai.getValueAt(index, 1));
        String kls = String.valueOf(tabelDataNilai.getValueAt(index, 2));
        String smt = String.valueOf(tabelDataNilai.getValueAt(index, 3));
        String gambar = String.valueOf(tabelDataNilai.getValueAt(index, 4));
        String hitung = String.valueOf(tabelDataNilai.getValueAt(index, 5));
        String baca = String.valueOf(tabelDataNilai.getValueAt(index, 6));
        String tulis = String.valueOf(tabelDataNilai.getValueAt(index, 7));
        String ngaji = String.valueOf(tabelDataNilai.getValueAt(index, 8));
        String rata = String.valueOf(tabelDataNilai.getValueAt(index, 9));
        String ket = String.valueOf(tabelDataNilai.getValueAt(index, 10));
        String tahun = String.valueOf(tabelDataNilai.getValueAt(index, 11));
        //mengambil nilai kedalam textfield
        idSiswaView.setText(id);
        namaSiswaView.setText(nama);
        kelasView.setText(kls);
        semesterView.setText(smt);
        nilaiMenggambarView.setText(gambar);
        nilaiBerhitungView.setText(hitung);
        nilaiMembacaView.setText(baca);
        nilaiMenulisView.setText(tulis);
        nilaiMengajiView.setText(ngaji);
        rataRataView.setText(rata);
        keteranganView.setText(ket);
        tahunAjaranView.setText(tahun);
        
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelDataNilai = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        idSiswaView = new javax.swing.JTextField();
        nilaiBerhitungView = new javax.swing.JTextField();
        nilaiMenggambarView = new javax.swing.JTextField();
        nilaiMembacaView = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        nilaiMenulisView = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        nilaiMengajiView = new javax.swing.JTextField();
        tahunAjaranView = new javax.swing.JTextField();
        cariSiswa = new javax.swing.JLabel();
        kelasView = new javax.swing.JTextField();
        semesterView = new javax.swing.JTextField();
        namaSiswaView = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        tombolInput = new javax.swing.JButton();
        tombolCancel = new javax.swing.JButton();
        tombolUpdate = new javax.swing.JButton();
        tombolDelete = new javax.swing.JButton();
        rataRataView = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        keteranganView = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jPanel1.setBackground(new java.awt.Color(0, 204, 204));

        tabelDataNilai.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12"
            }
        ));
        tabelDataNilai.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDataNilaiMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelDataNilai);

        jLabel1.setFont(new java.awt.Font("Source Code Pro Black", 1, 36)); // NOI18N
        jLabel1.setText("DATA NILAI");

        jLabel2.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel2.setText("ID Siswa");

        jLabel4.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel4.setText("Kelas");

        jLabel5.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel5.setText("Semester");

        jLabel6.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel6.setText("Nilai Menggambar");

        jLabel7.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel7.setText("Nilai Berhitung");

        jLabel8.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel8.setText("Nilai Membaca");

        idSiswaView.setEditable(false);
        idSiswaView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                idSiswaViewActionPerformed(evt);
            }
        });

        nilaiBerhitungView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nilaiBerhitungViewActionPerformed(evt);
            }
        });

        nilaiMenggambarView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nilaiMenggambarViewActionPerformed(evt);
            }
        });
        nilaiMenggambarView.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                nilaiMenggambarViewKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nilaiMenggambarViewKeyTyped(evt);
            }
        });

        nilaiMembacaView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nilaiMembacaViewActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel9.setText("Nilai Menulis");

        nilaiMenulisView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nilaiMenulisViewActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel10.setText("Nilai Mengaji");

        jLabel11.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel11.setText("Tahun Ajaran");

        nilaiMengajiView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nilaiMengajiViewActionPerformed(evt);
            }
        });

        tahunAjaranView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tahunAjaranViewActionPerformed(evt);
            }
        });

        cariSiswa.setBackground(new java.awt.Color(255, 153, 0));
        cariSiswa.setFont(new java.awt.Font("Courier New", 1, 24)); // NOI18N
        cariSiswa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/java_mvc/gambar/cari.png"))); // NOI18N
        cariSiswa.setText("Cari Siswa");
        cariSiswa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cariSiswaMouseClicked(evt);
            }
        });

        kelasView.setEditable(false);
        kelasView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kelasViewActionPerformed(evt);
            }
        });

        semesterView.setEditable(false);
        semesterView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semesterViewActionPerformed(evt);
            }
        });

        namaSiswaView.setEditable(false);
        namaSiswaView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaSiswaViewActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel13.setText("Nama Siswa");

        tombolInput.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tombolInput.setText("INPUT");
        tombolInput.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                tombolInputMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tombolInputMouseMoved(evt);
            }
        });
        tombolInput.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tombolInputMousePressed(evt);
            }
        });
        tombolInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolInputActionPerformed(evt);
            }
        });

        tombolCancel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tombolCancel.setText("CANCEL");
        tombolCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolCancelActionPerformed(evt);
            }
        });

        tombolUpdate.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tombolUpdate.setText("UPDATE");
        tombolUpdate.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                tombolUpdateMouseMoved(evt);
            }
        });
        tombolUpdate.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tombolUpdateMousePressed(evt);
            }
        });
        tombolUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUpdateActionPerformed(evt);
            }
        });

        tombolDelete.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        tombolDelete.setText("DELETE");
        tombolDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolDeleteActionPerformed(evt);
            }
        });

        rataRataView.setEditable(false);
        rataRataView.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        rataRataView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rataRataViewActionPerformed(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel12.setText("Rata-Rata");

        jLabel14.setFont(new java.awt.Font("Snap ITC", 0, 18)); // NOI18N
        jLabel14.setText("Keterangan");

        keteranganView.setEditable(false);
        keteranganView.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        keteranganView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keteranganViewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 832, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(108, 108, 108)
                        .addComponent(idSiswaView))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nilaiMenggambarView)
                            .addComponent(nilaiBerhitungView)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel13)
                            .addComponent(jLabel5))
                        .addGap(78, 78, 78)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(semesterView)
                            .addComponent(namaSiswaView)
                            .addComponent(kelasView)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cariSiswa)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(jLabel11))
                        .addGap(54, 54, 54)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tahunAjaranView)
                            .addComponent(nilaiMengajiView)
                            .addComponent(nilaiMembacaView)
                            .addComponent(nilaiMenulisView)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tombolCancel)
                            .addComponent(jLabel12))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(rataRataView, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(46, 46, 46)
                                .addComponent(jLabel14)
                                .addGap(18, 18, 18)
                                .addComponent(keteranganView))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(tombolDelete)
                                .addGap(18, 18, 18)
                                .addComponent(tombolUpdate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                                .addComponent(tombolInput)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(cariSiswa)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(idSiswaView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(namaSiswaView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(kelasView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(semesterView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(nilaiMenggambarView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(nilaiBerhitungView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(nilaiMembacaView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9)
                            .addComponent(nilaiMenulisView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(nilaiMengajiView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(tahunAjaranView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rataRataView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12)
                            .addComponent(jLabel14)
                            .addComponent(keteranganView, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tombolInput)
                            .addComponent(tombolCancel)
                            .addComponent(tombolUpdate)
                            .addComponent(tombolDelete)))
                    .addComponent(jScrollPane1))
                .addGap(17, 17, 17))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void idSiswaViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_idSiswaViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_idSiswaViewActionPerformed

    private void nilaiBerhitungViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nilaiBerhitungViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nilaiBerhitungViewActionPerformed

    private void nilaiMenggambarViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nilaiMenggambarViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nilaiMenggambarViewActionPerformed

    private void nilaiMembacaViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nilaiMembacaViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nilaiMembacaViewActionPerformed

    private void nilaiMenulisViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nilaiMenulisViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nilaiMenulisViewActionPerformed

    private void nilaiMengajiViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nilaiMengajiViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nilaiMengajiViewActionPerformed

    private void tahunAjaranViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tahunAjaranViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tahunAjaranViewActionPerformed

    private void kelasViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kelasViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_kelasViewActionPerformed

    private void semesterViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semesterViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_semesterViewActionPerformed

    private void namaSiswaViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaSiswaViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaSiswaViewActionPerformed

    private void tombolCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolCancelActionPerformed
        // TODO add your handling code here:
        cI.bersihkan();
    }//GEN-LAST:event_tombolCancelActionPerformed

    private void tombolUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUpdateActionPerformed
        // TODO add your handling code here:
        cI.update();
        tampilDataNilai();
    }//GEN-LAST:event_tombolUpdateActionPerformed

    private void cariSiswaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cariSiswaMouseClicked
        // TODO add your handling code here:
        viewListDataSiswaNilai vldsn = new viewListDataSiswaNilai(this);
        vldsn.setVisible(true);
    }//GEN-LAST:event_cariSiswaMouseClicked

    private void tombolInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolInputActionPerformed
        // TODO add your handling code here:
        cI.input();
        tampilDataNilai();
        
        
        
    }//GEN-LAST:event_tombolInputActionPerformed

    private void rataRataViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rataRataViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rataRataViewActionPerformed

    private void keteranganViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keteranganViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_keteranganViewActionPerformed

    private void nilaiMenggambarViewKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nilaiMenggambarViewKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_nilaiMenggambarViewKeyTyped

    private void nilaiMenggambarViewKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nilaiMenggambarViewKeyPressed
        // TODO add your handling code here:
         
    }//GEN-LAST:event_nilaiMenggambarViewKeyPressed

    private void tombolInputMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolInputMouseDragged
        // TODO add your handling code here:
       
    }//GEN-LAST:event_tombolInputMouseDragged

    private void tombolInputMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolInputMouseMoved
        // TODO add your handling code here:
    }//GEN-LAST:event_tombolInputMouseMoved

    private void tabelDataNilaiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDataNilaiMouseClicked
        // TODO add your handling code here:
        ambilDataTabel();
        cI.kontrolbutton2();
    }//GEN-LAST:event_tabelDataNilaiMouseClicked

    private void tombolUpdateMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolUpdateMouseMoved
        // TODO add your handling code here:
        
    }//GEN-LAST:event_tombolUpdateMouseMoved

    private void tombolDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolDeleteActionPerformed
        // TODO add your handling code here:
        cI.delete();
        tampilDataNilai();
    }//GEN-LAST:event_tombolDeleteActionPerformed

    private void tombolUpdateMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolUpdateMousePressed
        // TODO add your handling code here:
        total = Integer.parseInt(nilaiMembacaView.getText()) + Integer.parseInt(nilaiBerhitungView.getText()) + 
                Integer.parseInt(nilaiMengajiView.getText()) + Integer.parseInt(nilaiMenggambarView.getText()) +
                Integer.parseInt(nilaiMenulisView.getText());
        average=total/5;
        rataRataView.setText(Float.toString(average));
        
        if(average<=20){
            keteranganView.setText("E");
        }
        else if (average<=40){
            keteranganView.setText("D");
        }
        else if (average<=60){
            keteranganView.setText("C");
        }
        else if (average<=80){
            keteranganView.setText("B");
        }
        else if (average<=100 || average>100){
            keteranganView.setText("A");
        }
    }//GEN-LAST:event_tombolUpdateMousePressed

    private void tombolInputMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tombolInputMousePressed
        // TODO add your handling code here:
        total = Integer.parseInt(nilaiMembacaView.getText()) + Integer.parseInt(nilaiBerhitungView.getText()) + 
                Integer.parseInt(nilaiMengajiView.getText()) + Integer.parseInt(nilaiMenggambarView.getText()) +
                Integer.parseInt(nilaiMenulisView.getText());
        average=total/5;
        rataRataView.setText(Float.toString(average));
        
        if(average<=20){
            keteranganView.setText("E");
        }
        else if (average<=40){
            keteranganView.setText("D");
        }
        else if (average<=60){
            keteranganView.setText("C");
        }
        else if (average<=80){
            keteranganView.setText("B");
        }
        else if (average<=100 || average>100){
            keteranganView.setText("A");
        }
    }//GEN-LAST:event_tombolInputMousePressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel cariSiswa;
    private javax.swing.JTextField idSiswaView;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField kelasView;
    private javax.swing.JTextField keteranganView;
    private javax.swing.JTextField namaSiswaView;
    private javax.swing.JTextField nilaiBerhitungView;
    private javax.swing.JTextField nilaiMembacaView;
    private javax.swing.JTextField nilaiMengajiView;
    private javax.swing.JTextField nilaiMenggambarView;
    private javax.swing.JTextField nilaiMenulisView;
    private javax.swing.JTextField rataRataView;
    private javax.swing.JTextField semesterView;
    private javax.swing.JTable tabelDataNilai;
    private javax.swing.JTextField tahunAjaranView;
    private javax.swing.JButton tombolCancel;
    private javax.swing.JButton tombolDelete;
    private javax.swing.JButton tombolInput;
    private javax.swing.JButton tombolUpdate;
    // End of variables declaration//GEN-END:variables
}
