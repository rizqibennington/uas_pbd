/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.mysql.jdbc.Statement;
import com.toedter.calendar.JDateChooser;
import controller.controllerGuru;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import db.koneksiDatabase;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.modelGuru;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author AST-PC
 */
public class viewGuru extends javax.swing.JInternalFrame {
//    private controllerPelanggan cP;
    /**
     * Creates new form ViewPelangganInternal
     */
    private controllerGuru cG;
    private DefaultTableModel model;
    
    public viewGuru() {
        initComponents();
        cG = new controllerGuru(this);
        model = new DefaultTableModel();
        tabelDataGuru.setModel(model);
        model.addColumn("NIP");
        model.addColumn("Nama");
        model.addColumn("Nomor HP");
        model.addColumn("Jenis Kelamin");
        model.addColumn("Tanggal Lahir");
        model.addColumn("Alamat");
        model.addColumn("Agama");
        model.addColumn("Mengajar");

        tampilDataGuru();
        cG.kontrolbutton2();
    }

    public JComboBox getAgama() {
        return agama;
    }

    public JTextArea getAlamat() {
        return alamat;
    }

    public JComboBox getMengajar() {
        return mengajar;
    }

    public JTextField getNamaGuru() {
        return namaGuru;
    }

    public JTextField getNip() {
        return nip;
    }

    public JTextField getNomorHp() {
        return nomorHp;
    }

    public JDateChooser getTanggalLahir() {
        return tanggalLahir;
    }

   

   

    public JRadioButton getPria() {
        return pria;
    }

    public JButton getTombolBatal() {
        return tombolBatal;
    }

    public JButton getTombolHapus() {
        return tombolHapus;
    }

    public JButton getTombolSimpan() {
        return tombolSimpan;
    }

    public JButton getTombolUbah() {
        return tombolUbah;
    }

    public JRadioButton getWanita() {
        return wanita;
    }

    

    
    
    
    private void tampilDataGuru(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
//        
        String sql = "SELECT * FROM guru";
//        
        try {
            Statement stat = (Statement) koneksiDatabase.getKoneksi().createStatement();
            ResultSet res = stat.executeQuery(sql);
            
            
            while (res.next()){
                //mengambil hasil query variabel sql
                Object[] hasil;
                hasil = new Object[8];//karena ada 8 field
                hasil[0] = res.getString("nip");
                hasil[1] = res.getString("nama");
                hasil[2] = res.getString("nomorHp");
                hasil[3] = res.getString("jenisKelamin");
                hasil[4] = res.getString("tanggalLahir");
                hasil[5] = res.getString("alamat");
                hasil[6] = res.getString("agama");
                hasil[7] = res.getString("mengajar");
                model.addRow(hasil);
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(viewGuru.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void ambilDataTabel(){
        
        int index = tabelDataGuru.getSelectedRow();
        //mengambul nilai dari tabel
        String nipguru = String.valueOf(tabelDataGuru.getValueAt(index, 0));
        String namaguru = String.valueOf(tabelDataGuru.getValueAt(index, 1));
        String hp = String.valueOf(tabelDataGuru.getValueAt(index, 2));
        String jk = String.valueOf(tabelDataGuru.getValueAt(index, 3));
        String almt = String.valueOf(tabelDataGuru.getValueAt(index, 5));
        String agm = String.valueOf(tabelDataGuru.getValueAt(index, 6));
        String ngajar = String.valueOf(tabelDataGuru.getValueAt(index, 7));
//        
//        //mengambil nilai kedalam textfield
        nip.setText(nipguru);
        namaGuru.setText(namaguru);
        nomorHp.setText(hp);
        if (jk.equals("Pria")){
            pria.setSelected(true);
        }else {
            wanita.setSelected(true);
        }
        alamat.setText(almt);
        agama.setSelectedItem(agm);
        mengajar.setSelectedItem(ngajar);
        try {          
            DefaultTableModel modelInven = (DefaultTableModel)tabelDataGuru.getModel();
            //ambil nilainya dari tabel                    
            java.util.Date tgl = new SimpleDateFormat("yyyy-MM-dd").parse((String)modelInven.getValueAt(index, 4).toString());           
            //mengisi nilai kedalam textfield                       
            tanggalLahir.setDate(tgl);                     
        } catch (ParseException ex) {
            Logger.getLogger(viewGuru.class.getName()).log(Level.SEVERE, null, ex);
        }
//        cP.kontrolButtonDua();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        namaGuru = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        nomorHp = new javax.swing.JTextField();
        pria = new javax.swing.JRadioButton();
        wanita = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        alamat = new javax.swing.JTextArea();
        tombolSimpan = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        nip = new javax.swing.JTextField();
        tombolUbah = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolBatal = new javax.swing.JButton();
        agama = new javax.swing.JComboBox<String>();
        mengajar = new javax.swing.JComboBox<String>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelDataGuru = new javax.swing.JTable();
        tanggalLahir = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Modul Data Guru");

        jPanel1.setBackground(new java.awt.Color(255, 204, 0));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel6.setText("Data Guru");

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel1.setText("Nama Guru");

        namaGuru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaGuruActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel2.setText("No Hp");

        nomorHp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nomorHpActionPerformed(evt);
            }
        });

        pria.setBackground(new java.awt.Color(255, 204, 0));
        buttonGroup1.add(pria);
        pria.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        pria.setText("Pria");

        wanita.setBackground(new java.awt.Color(255, 204, 0));
        buttonGroup1.add(wanita);
        wanita.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        wanita.setText("Wanita");

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel3.setText("Jenis Kelamin");

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel4.setText("Tanggal Lahir");

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel5.setText("Alamat");

        alamat.setColumns(20);
        alamat.setRows(5);
        jScrollPane1.setViewportView(alamat);

        tombolSimpan.setBackground(new java.awt.Color(255, 255, 153));
        tombolSimpan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        jLabel7.setText("NIP");

        tombolUbah.setBackground(new java.awt.Color(255, 255, 153));
        tombolUbah.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolUbah.setText("Ubah");
        tombolUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUbahActionPerformed(evt);
            }
        });

        tombolHapus.setBackground(new java.awt.Color(255, 255, 153));
        tombolHapus.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolHapus.setText("Hapus");
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolBatal.setBackground(new java.awt.Color(255, 255, 153));
        tombolBatal.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolBatal.setText("Batal");
        tombolBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolBatalActionPerformed(evt);
            }
        });

        agama.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Islam", "Kristen", "Hindu", "Budha" }));

        mengajar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Membaca", "Menulis", "Berhitung", "Menggambar", "Mengaji" }));

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel12.setText("Agama");

        jLabel13.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel13.setText("Mengajar");

        tabelDataGuru.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8"
            }
        ));
        tabelDataGuru.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDataGuruMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tabelDataGuru);

        tanggalLahir.setDateFormatString("d MMMM yyyy");

        jButton1.setText("PRINT");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel7)
                                    .addComponent(tombolBatal))
                                .addGap(12, 12, 12)
                                .addComponent(tombolHapus))
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(namaGuru)
                            .addComponent(jScrollPane1)
                            .addComponent(nomorHp)
                            .addComponent(nip)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(35, 35, 35)
                                .addComponent(tombolUbah)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tombolSimpan))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(pria)
                                                .addGap(18, 18, 18)
                                                .addComponent(wanita))
                                            .addComponent(mengajar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(tanggalLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(469, 469, 469)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(nip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(namaGuru, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(nomorHp, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pria)
                                .addComponent(wanita)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(13, 13, 13))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(tanggalLahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(agama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(mengajar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tombolBatal)
                            .addComponent(tombolUbah)
                            .addComponent(tombolSimpan)
                            .addComponent(tombolHapus)))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void namaGuruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaGuruActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaGuruActionPerformed

    private void nomorHpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nomorHpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nomorHpActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
          cG.simpanDataGuru();
          cG.bersihkan();
          tampilDataGuru();
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUbahActionPerformed
        // TODO add your handling code here:
        cG.ubahDataGuru();
        cG.bersihkan();
        tampilDataGuru();
        
    }//GEN-LAST:event_tombolUbahActionPerformed

    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        // TODO add your handling code here:
        cG.hapusData();
        cG.bersihkan();
        tampilDataGuru();
        
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolBatalActionPerformed
        // TODO add your handling code here:
        cG.bersihkan();
    }//GEN-LAST:event_tombolBatalActionPerformed

    private void tabelDataGuruMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDataGuruMouseClicked
        // TODO add your handling code here:
        ambilDataTabel();
        cG.kontrolbutton();
    }//GEN-LAST:event_tabelDataGuruMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try {
        JasperPrint jp = JasperFillManager.fillReport(getClass().getResourceAsStream("guru.jasper"), null, koneksiDatabase.getKoneksi());
        JasperViewer.viewReport(jp, false);
        } catch(Exception e) {
            JOptionPane.showMessageDialog(rootPane, e);
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> agama;
    private javax.swing.JTextArea alamat;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JComboBox<String> mengajar;
    private javax.swing.JTextField namaGuru;
    private javax.swing.JTextField nip;
    private javax.swing.JTextField nomorHp;
    private javax.swing.JRadioButton pria;
    private javax.swing.JTable tabelDataGuru;
    private com.toedter.calendar.JDateChooser tanggalLahir;
    private javax.swing.JButton tombolBatal;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolUbah;
    private javax.swing.JRadioButton wanita;
    // End of variables declaration//GEN-END:variables
}
