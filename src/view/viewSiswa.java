/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import com.mysql.jdbc.Statement;
import com.toedter.calendar.JDateChooser;
import controller.controllerSiswa;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import db.koneksiDatabase;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author AST-PC
 */
public class viewSiswa extends javax.swing.JInternalFrame {
//    private controllerPelanggan cP;
    /**
     * Creates new form ViewPelangganInternal
     */
    private controllerSiswa cS;
    private DefaultTableModel model;
    
    public viewSiswa() {
        initComponents();
        cS = new controllerSiswa(this);
        model = new DefaultTableModel();
        tabelDataSiswa.setModel(model);
        model.addColumn("ID");
        model.addColumn("Nama");
        model.addColumn("Jenis Kelamin");
        model.addColumn("Tanggal Lahir");
        model.addColumn("Alamat");
        model.addColumn("Agama");
        model.addColumn("Kelas");
        model.addColumn("Semester");
        model.addColumn("Ortu/Wali");
        model.addColumn("Hp Ortu/Wali");
//
        tampilDataSiswa();
        cS.kontrolbutton2();
    }

    public JComboBox<String> getAgamaView() {
        return agamaView;
    }

    public JTextArea getAlamatView() {
        return alamatView;
    }

    public JTextField getIdSiswaView() {
        return idSiswaView;
    }

    public JComboBox<String> getKelasView() {
        return kelasView;
    }

    public JTextField getNamaSiswaView() {
        return namaSiswaView;
    }

    public JTextField getNamaWaliView() {
        return namaWaliView;
    }

    public JTextField getNoHpWaliView() {
        return noHpWaliView;
    }

    public JRadioButton getPria() {
        return pria;
    }

    public JDateChooser getTanggalLahir() {
        return tanggalLahir;
    }


    public JButton getTombolBatal() {
        return tombolBatal;
    }

    public JButton getTombolHapus() {
        return tombolHapus;
    }

    public JButton getTombolSimpan() {
        return tombolSimpan;
    }

    public JButton getTombolUbah() {
        return tombolUbah;
    }

    public JRadioButton getWanita() {
        return wanita;
    }

    public JTextField getSemester() {
        return semester;
    }


    

    
    
    
    private void tampilDataSiswa(){
        model.getDataVector().removeAllElements();
        model.fireTableDataChanged();
//        
        String sql = "SELECT * FROM siswa";
//        
        try {
            Statement stat = (Statement) koneksiDatabase.getKoneksi().createStatement();
            ResultSet res = stat.executeQuery(sql);
            
            
            while (res.next()){
                //mengambil hasil query variabel sql
                Object[] hasil;
                hasil = new Object[10];//karena ada 8 field
                hasil[0] = res.getString("idSiswa");
                hasil[1] = res.getString("namaSiswa");
                hasil[2] = res.getString("jenisKelamin");
                hasil[3] = res.getString("tanggalLahir");
                hasil[4] = res.getString("alamat");
                hasil[5] = res.getString("agama");
                hasil[6] = res.getString("kelas");
                hasil[7] = res.getString("semester");
                hasil[8] = res.getString("namaWali");
                hasil[9] = res.getString("noHpWali");
                model.addRow(hasil);
            }
                    
        } catch (SQLException ex) {
            Logger.getLogger(viewSiswa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    private void ambilDataTabel(){
        
        int index = tabelDataSiswa.getSelectedRow();
        //mengambul nilai dari tabel
        String id = String.valueOf(tabelDataSiswa.getValueAt(index, 0));
        String namasis = String.valueOf(tabelDataSiswa.getValueAt(index, 1));
        String jk = String.valueOf(tabelDataSiswa.getValueAt(index, 2));
        String almt = String.valueOf(tabelDataSiswa.getValueAt(index, 4));
        String agm = String.valueOf(tabelDataSiswa.getValueAt(index, 5));
        String kls = String.valueOf(tabelDataSiswa.getValueAt(index, 6));
        String smt = String.valueOf(tabelDataSiswa.getValueAt(index, 7));
        String namawal = String.valueOf(tabelDataSiswa.getValueAt(index, 8));
        String hp = String.valueOf(tabelDataSiswa.getValueAt(index, 9));
////        
        //mengambil nilai kedalam textfield
        idSiswaView.setText(id);
        namaSiswaView.setText(namasis);
        if (jk.equals("Pria")){
            pria.setSelected(true);
        }else {
            wanita.setSelected(true);
        }
        alamatView.setText(almt);
        agamaView.setSelectedItem(agm);
        kelasView.setSelectedItem(kls);
        semester.setText(smt);
        namaWaliView.setText(namawal);
        noHpWaliView.setText(hp);
        
        try {          
            DefaultTableModel modelInven = (DefaultTableModel)tabelDataSiswa.getModel();
            //ambil nilainya dari tabel                    
            java.util.Date tgl = new SimpleDateFormat("yyyy-MM-dd").parse((String)modelInven.getValueAt(index, 3).toString());           
            //mengisi nilai kedalam textfield                       
            tanggalLahir.setDate(tgl);                     
        } catch (ParseException ex) {
            Logger.getLogger(viewSiswa.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
 
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        namaSiswaView = new javax.swing.JTextField();
        pria = new javax.swing.JRadioButton();
        wanita = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        alamatView = new javax.swing.JTextArea();
        tombolSimpan = new javax.swing.JButton();
        idSiswaView = new javax.swing.JTextField();
        tombolUbah = new javax.swing.JButton();
        tombolHapus = new javax.swing.JButton();
        tombolBatal = new javax.swing.JButton();
        agamaView = new javax.swing.JComboBox<String>();
        kelasView = new javax.swing.JComboBox<String>();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabelDataSiswa = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        noHpWaliView = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        namaWaliView = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        semester = new javax.swing.JTextField();
        tanggalLahir = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Modul Data Siswa");

        jPanel1.setBackground(new java.awt.Color(102, 204, 0));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel6.setFont(new java.awt.Font("Comic Sans MS", 0, 18)); // NOI18N
        jLabel6.setText("Data Siswa");

        jLabel1.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel1.setText("Nama Siswa");

        namaSiswaView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaSiswaViewActionPerformed(evt);
            }
        });

        pria.setBackground(new java.awt.Color(102, 204, 0));
        buttonGroup1.add(pria);
        pria.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        pria.setText("Pria");

        wanita.setBackground(new java.awt.Color(102, 204, 0));
        buttonGroup1.add(wanita);
        wanita.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        wanita.setText("Wanita");

        jLabel3.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel3.setText("Jenis Kelamin");

        jLabel4.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel4.setText("Tanggal Lahir");

        jLabel5.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel5.setText("Alamat");

        alamatView.setColumns(20);
        alamatView.setRows(5);
        jScrollPane1.setViewportView(alamatView);

        tombolSimpan.setBackground(new java.awt.Color(255, 255, 153));
        tombolSimpan.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolSimpan.setText("Simpan");
        tombolSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolSimpanActionPerformed(evt);
            }
        });

        tombolUbah.setBackground(new java.awt.Color(255, 255, 153));
        tombolUbah.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolUbah.setText("Ubah");
        tombolUbah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolUbahActionPerformed(evt);
            }
        });

        tombolHapus.setBackground(new java.awt.Color(255, 255, 153));
        tombolHapus.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolHapus.setText("Hapus");
        tombolHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolHapusActionPerformed(evt);
            }
        });

        tombolBatal.setBackground(new java.awt.Color(255, 255, 153));
        tombolBatal.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        tombolBatal.setText("Batal");
        tombolBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tombolBatalActionPerformed(evt);
            }
        });

        agamaView.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Islam", "Kristen", "Hindu", "Budha" }));

        kelasView.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A1", "A2", "B1", "B2" }));

        jLabel12.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel12.setText("Agama");

        jLabel13.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel13.setText("Kelas");

        tabelDataSiswa.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8"
            }
        ));
        tabelDataSiswa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelDataSiswaMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tabelDataSiswa);

        jLabel8.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel8.setText("ID Siswa");

        jLabel7.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel7.setText("Orangtua/Wali");

        noHpWaliView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                noHpWaliViewActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel9.setText("No HP Ortu/Wali");

        namaWaliView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                namaWaliViewActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Comic Sans MS", 0, 12)); // NOI18N
        jLabel14.setText("Semester");

        semester.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                semesterActionPerformed(evt);
            }
        });

        tanggalLahir.setDateFormatString("d MMMM yyyy");

        jButton1.setText("PRINT");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel5)
                                            .addComponent(jLabel13)
                                            .addComponent(jLabel8))
                                        .addGap(86, 86, 86)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(namaSiswaView)
                                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                                            .addComponent(idSiswaView)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel4))
                                        .addGap(78, 78, 78)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addComponent(pria)
                                                .addGap(18, 18, 18)
                                                .addComponent(wanita)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(tanggalLahir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel12)
                                            .addComponent(jLabel7)
                                            .addComponent(jLabel9)
                                            .addComponent(jLabel14))
                                        .addGap(53, 53, 53)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(noHpWaliView)
                                            .addComponent(namaWaliView)
                                            .addComponent(semester, javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(agamaView, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(kelasView, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(tombolBatal)
                                .addGap(27, 27, 27)
                                .addComponent(tombolHapus)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(tombolUbah)
                                .addGap(32, 32, 32)
                                .addComponent(tombolSimpan)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(469, 469, 469)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(idSiswaView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(namaSiswaView, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(pria)
                                .addComponent(wanita)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(tanggalLahir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(agamaView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(kelasView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                                .addComponent(semester, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(namaWaliView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(noHpWaliView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tombolSimpan)
                            .addComponent(tombolUbah)
                            .addComponent(tombolHapus)
                            .addComponent(tombolBatal)))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void namaSiswaViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaSiswaViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaSiswaViewActionPerformed

    private void tombolSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolSimpanActionPerformed
          cS.simpanDataSiswa();
          cS.bersihkan();
          tampilDataSiswa();
    }//GEN-LAST:event_tombolSimpanActionPerformed

    private void tombolUbahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolUbahActionPerformed
        // TODO add your handling code here:
        cS.ubahDataSiswa();
        cS.bersihkan();
        tampilDataSiswa();
        
    }//GEN-LAST:event_tombolUbahActionPerformed

    private void tombolHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolHapusActionPerformed
        // TODO add your handling code here:
        cS.hapusDataSiswa();
        cS.bersihkan();
        tampilDataSiswa();
    }//GEN-LAST:event_tombolHapusActionPerformed

    private void tombolBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tombolBatalActionPerformed
        // TODO add your handling code here:
        cS.bersihkan();
    }//GEN-LAST:event_tombolBatalActionPerformed

    private void tabelDataSiswaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelDataSiswaMouseClicked
        // TODO add your handling code here:
        ambilDataTabel();
        cS.kontrolbutton();
//        cG.kontrolbutton();
    }//GEN-LAST:event_tabelDataSiswaMouseClicked

    private void noHpWaliViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_noHpWaliViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_noHpWaliViewActionPerformed

    private void namaWaliViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_namaWaliViewActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_namaWaliViewActionPerformed

    private void semesterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_semesterActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_semesterActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here: 
        try {
        JasperPrint jp = JasperFillManager.fillReport(getClass().getResourceAsStream("siswa.jasper"), null, koneksiDatabase.getKoneksi());
        JasperViewer.viewReport(jp, false);
        } catch(Exception e) {
            JOptionPane.showMessageDialog(rootPane, e);
        }
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> agamaView;
    private javax.swing.JTextArea alamatView;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JTextField idSiswaView;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JComboBox<String> kelasView;
    private javax.swing.JTextField namaSiswaView;
    private javax.swing.JTextField namaWaliView;
    private javax.swing.JTextField noHpWaliView;
    private javax.swing.JRadioButton pria;
    private javax.swing.JTextField semester;
    private javax.swing.JTable tabelDataSiswa;
    private com.toedter.calendar.JDateChooser tanggalLahir;
    private javax.swing.JButton tombolBatal;
    private javax.swing.JButton tombolHapus;
    private javax.swing.JButton tombolSimpan;
    private javax.swing.JButton tombolUbah;
    private javax.swing.JRadioButton wanita;
    // End of variables declaration//GEN-END:variables
}
